//
//  ViewController.swift
//  FoodTaskerMobile
//
//  Created by Leo Trieu on 9/20/16.
//  Copyright © 2016 Leo Trieu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBOutlet weak var EmailTF: UITextField!
    @IBOutlet weak var PasswordTF: UITextField!
    
    
    @IBAction func LoginButton(_ sender: Any) {
        let email = EmailTF.text!
        let password = PasswordTF.text!
        if(email.isEmpty || password.isEmpty){
            //display alert message
    
            let alertController = UIAlertController(title: "Warning", message:"Please input email and password", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
    }
}

